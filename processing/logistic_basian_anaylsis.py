import pandas as pd
import os
import numpy as np

stats_path = '../stats'
files = [os.path.join(stats_path, name) for name in os.listdir(stats_path) if name.endswith('.csv')]

dataframes = []

for file_path in files:
    df = pd.read_csv(file_path, encoding='utf-8', encoding_errors='ignore')

    df['Win'] = df['W/L'].apply(lambda x: 1 if 'W' in x else 0)
    opp_enc = pd.get_dummies(df['Opp'], prefix='Opp')
    dn_enc = pd.get_dummies(df['D/N'], prefix='DN')

    df_preprocessed = pd.concat([df, opp_enc, dn_enc], axis=1)

    dataframes.append(df_preprocessed)

final_dfs = pd.concat(dataframes, ignore_index=True)

important_features = ['R', 'RA', 'cLI'] + [col for col in final_dfs if col.startswith('Opp_') or col.startswith('DN_')]

X = final_dfs[important_features].values
y = final_dfs['Win'].values

print(X)